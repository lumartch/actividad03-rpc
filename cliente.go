package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"net/rpc"
	"os"
)

func leerString() string {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	return scanner.Text()
}

func main() {
	c, err := rpc.Dial("tcp", ":9999")
	if err != nil {
		fmt.Println(err)
		return
	}
	var opc string
	for opc != "0" {
		fmt.Print("\033[H\033[2J")
		fmt.Println("+-------------------------------+")
		fmt.Println("|  Administración de alumnos    |")
		fmt.Println("+-------------------------------+")
		fmt.Println("| 1.- Agregar calificación.     |")
		fmt.Println("| 2.- Obtener el promedio       |")
		fmt.Println("|     del alumno.               |")
		fmt.Println("| 3.- Obtener el promedio de    |")
		fmt.Println("|     todos los alumnos.        |")
		fmt.Println("| 4.- Obtener el promedio       |")
		fmt.Println("|     por materia.              |")
		fmt.Println("| 0.- Salir.                    |")
		fmt.Println("+-------------------------------+")
		fmt.Println("| Opción:                       |")
		fmt.Println("+-------------------------------+")
		fmt.Print("\033[13;11H")
		opc = leerString()
		switch opc {
		case "1":
			// Se crea un struct para envíar los datos al servidor
			st := struct {
				Alumno       string
				Materia      string
				Calificacion float64
			}{}
			// Se capturan los datos
			fmt.Println("+-------------------------------+")
			fmt.Println("| Alumno:                       |")
			fmt.Println("+-------------------------------+")
			fmt.Print("\033[15;11H")
			st.Alumno = leerString()
			fmt.Println("+-------------------------------+")
			fmt.Println("| Materia:                      |")
			fmt.Println("+-------------------------------+")
			fmt.Print("\033[17;12H")
			st.Materia = leerString()
			fmt.Println("+-------------------------------+")
			fmt.Println("| Calificación:                 |")
			fmt.Println("+-------------------------------+")
			fmt.Print("\033[19;17H")
			fmt.Scanln(&st.Calificacion)
			// Se codifican los datos en un JSON para enviarse
			bs, err := json.Marshal(st)
			if err != nil {
				fmt.Println(err)
			}
			// En caso de que no haya problema al codificar se envía al servidor
			var mensaje string
			err = c.Call("Server.RegistrarCalificacion", bs, &mensaje)
			if err != nil {
				fmt.Println("+-------------------------------+")
				fmt.Println(err)
				fmt.Println("+-------------------------------+")
			} else {
				fmt.Println("+-------------------------------+")
				fmt.Println(mensaje)
				fmt.Println("+-------------------------------+")
			}
		case "2":
			var alumno string
			// Se capturan los datos
			fmt.Println("+-------------------------------+")
			fmt.Println("| Alumno:                       |")
			fmt.Println("+-------------------------------+")
			fmt.Print("\033[15;11H")
			alumno = leerString()
			var promedio string
			err = c.Call("Server.PromedioAlumno", alumno, &promedio)
			if err != nil {
				fmt.Println("+-------------------------------+")
				fmt.Println(err)
				fmt.Println("+-------------------------------+")
			} else {
				fmt.Print("\033[H\033[2J")
				fmt.Println(promedio)
			}
		case "3":
			var promedio string
			err = c.Call("Server.PromedioTodos", "Promedio total", &promedio)
			if err != nil {
				fmt.Println("+-------------------------------+")
				fmt.Println(err)
				fmt.Println("+-------------------------------+")
			} else {
				fmt.Print("\033[H\033[2J")
				fmt.Println(promedio)
			}
		case "4":
			var materia string
			// Se capturan los datos
			fmt.Println("+-------------------------------+")
			fmt.Println("| Materia:                      |")
			fmt.Println("+-------------------------------+")
			fmt.Print("\033[15;12H")
			materia = leerString()
			var promedio string
			err = c.Call("Server.PromedioMateria", materia, &promedio)
			if err != nil {
				fmt.Println("+-------------------------------+")
				fmt.Println(err)
				fmt.Println("+-------------------------------+")
			} else {
				fmt.Print("\033[H\033[2J")
				fmt.Println(promedio)
			}
		case "0":
			fmt.Println("+-------------------------------+")
			fmt.Println("| Gracias por usar la app.      |")
			fmt.Println("|    Vuelva pronto :D           |")
			fmt.Println("+-------------------------------+")
		}
		fmt.Print("\nPresione [enter] para continuar...")
		fmt.Scanln()
	}
}
