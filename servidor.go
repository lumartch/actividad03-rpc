package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"net/rpc"
)

type Server struct {
	Materias map[string]map[string]float64
	Alumnos  map[string]map[string]float64
}

func (s *Server) RegistrarCalificacion(jsnData []byte, reply *string) error {
	var jsnReciever struct {
		Materia      string
		Alumno       string
		Calificacion float64
	}
	// Revisa error en el JSON
	err := json.Unmarshal(jsnData, &jsnReciever)
	if err != nil {
		return errors.New("|Datos corruptos, ingreselos    |\n| nuevamente.                    |\n")
	}
	// Verifica si la materia ya existe en el diccionario de datos
	if v, ok := s.Materias[jsnReciever.Materia]; ok {
		// En caso de existir la materia, se busca si existe el alumno.
		if _, ok_2 := v[jsnReciever.Alumno]; ok_2 {
			// Fracaso en la operación si ya existe el alumno
			return errors.New("| El alumno ya tiene registrada |\n| una calificación en esta      |\n| materia.                      |\n| Intente de nuevo.             |\n")
		}
	}
	// Éxito en la operación, se registra en el map de Materias
	if s.Materias[jsnReciever.Materia] == nil {
		s.Materias[jsnReciever.Materia] = map[string]float64{}
	}
	s.Materias[jsnReciever.Materia][jsnReciever.Alumno] = jsnReciever.Calificacion
	// Éxito en la operación, se registra en el map de Alumnos
	if s.Alumnos[jsnReciever.Alumno] == nil {
		s.Alumnos[jsnReciever.Alumno] = map[string]float64{}
	}
	s.Alumnos[jsnReciever.Alumno][jsnReciever.Materia] = jsnReciever.Calificacion
	*reply = "| Se ha registrado correctamente|\n| la calificación del alumno.   |"
	return nil
}

func (s *Server) PromedioAlumno(alumno string, reply *string) error {
	if v, ok := s.Alumnos[alumno]; ok {
		var promedio float64
		i := 0.0
		*reply = fmt.Sprintln("+------------------------------------------------+")
		*reply += fmt.Sprintln("| Alumno: ", alumno)
		for materia, calificacion := range v {
			*reply += fmt.Sprintln("+------------------------------------------------+")
			*reply += fmt.Sprintln("| Materia: ", materia)
			*reply += fmt.Sprintln("| Calificación: ", calificacion)
			promedio += calificacion
			i++
		}
		promedio = promedio / i
		*reply += fmt.Sprintln("+------------------------------------------------+")
		*reply += fmt.Sprintln("| Promedio: ", promedio)
		*reply += fmt.Sprintln("+------------------------------------------------+")
		return nil
	}
	return errors.New("| El alumno no existe.          |\n| Intente con otro nombre.      |")
}

func (s *Server) PromedioTodos(msg string, reply *string) error {
	if s.Alumnos == nil {
		return errors.New("| No hay materias ni alumnos    |\n| regitrados aún.               |\n| Intente más tarde.            |\n")
	}
	promAlumnos := []float64{}
	*reply = fmt.Sprintln("+----------------------------------+")
	for alumno, alumCal := range s.Alumnos {
		i := 0.0
		promedio := 0.0
		for _, calificacion := range alumCal {
			promedio += calificacion
			i++
		}
		promedio = promedio / i
		promAlumnos = append(promAlumnos, promedio)
		*reply += fmt.Sprintln("| Alumno: ", alumno, "| Promedio: ", promedio)
		*reply += fmt.Sprintln("+----------------------------------+")
	}
	promGeneral := 0.0
	i := 0.0
	for _, v := range promAlumnos {
		promGeneral += v
		i++
	}
	promGeneral = promGeneral / i
	*reply += fmt.Sprintln("| Promedio general: ", promGeneral)
	*reply += fmt.Sprintln("+----------------------------------+")
	return nil
}

func (s *Server) PromedioMateria(materia string, reply *string) error {
	if v, ok := s.Materias[materia]; ok {
		var promedio float64
		i := 0.0
		*reply = fmt.Sprintln("+------------------------------------------------+")
		*reply += fmt.Sprintln("| Materia: ", materia)
		for alumno, calificacion := range v {
			*reply += fmt.Sprintln("+------------------------------------------------+")
			*reply += fmt.Sprintln("| Alumno: ", alumno)
			*reply += fmt.Sprintln("| Calificación: ", calificacion)
			promedio += calificacion
			i++
		}
		promedio = promedio / i
		*reply += fmt.Sprintln("+------------------------------------------------+")
		*reply += fmt.Sprintln("| Promedio: ", promedio)
		*reply += fmt.Sprintln("+------------------------------------------------+")
		return nil
	}
	return errors.New("| La materia no existe.         |\n| Intente con otra aignatura.   |")
}

func servidor() {
	s := Server{
		Materias: map[string]map[string]float64{},
		Alumnos:  map[string]map[string]float64{},
	}
	sr := &s
	rpc.Register(sr)
	ln, err := net.Listen("tcp", ":9999")
	if err != nil {
		fmt.Println(err)
	}
	for {
		c, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}
		go rpc.ServeConn(c)
	}
}

func main() {
	go servidor()
	var input string
	fmt.Scanln(&input)
}
